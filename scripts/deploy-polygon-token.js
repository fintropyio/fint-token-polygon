const { ethers, upgrades } = require("hardhat");
const { getImplementationAddress, Manifest } = require('@openzeppelin/upgrades-core');

async function main() {
    const manifestClient = await Manifest.forNetwork(network.provider);
    const manifest = await manifestClient.read();
    const signer = (await ethers.getSigners())[0];
    const argData= {
        // matic
        137: {
            childManager: "0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa",
            tokenReceiver: "0x6B9A645b067220d390D1Fcb8E349208B1cb16206",
            antisnipe: "0x2306b42Baca1b34770EA489F4eC2023f5340B0Ca",
            restrictor: "0x9841BBb35BDeeDDD737103E02eD97a525b3FA85C"
        },
        // matic mumbai
        80001: {
            childManager: "0xb5505a6d998549090530911180f38aC5130101c6",
            tokenReceiver: "0x6B9A645b067220d390D1Fcb8E349208B1cb16206",
            antisnipe: "0x2306b42Baca1b34770EA489F4eC2023f5340B0Ca",
            restrictor: "0x9841BBb35BDeeDDD737103E02eD97a525b3FA85C"
        },
        // local
        31337: {
            childManager: "0xb5505a6d998549090530911180f38aC5130101c6",
            tokenReceiver: signer.address,
            antisnipe: "0x2306b42Baca1b34770EA489F4eC2023f5340B0Ca",
            restrictor: "0x9841BBb35BDeeDDD737103E02eD97a525b3FA85C"
        }

    }
    let proxyAddr;
    let net = await ethers.provider.getNetwork();

    if (manifest.proxies[0] && net.chainId!=311337) {
        proxyAddr = manifest.proxies[0].address;
        console.log("proxy already deployed, reusing!", proxyAddr);
        await upgrades.upgradeProxy(proxyAddr, await ethers.getContractFactory("FintTokenPOS"));
    } else {
        const Token = await ethers.getContractFactory("FintTokenPOS");
        const args = argData[net.chainId]
        const token = await upgrades.deployProxy(Token, [
            args.childManager, 
            args.tokenReceiver,
            args.antisnipe, 
            args.restrictor
        ]);
        await token.deployed();
        proxyAddr = token.address;

        console.log("Token proxy deployed to:", proxyAddr);
    }

    
    const currentImplAddress = await getImplementationAddress(ethers.provider, proxyAddr);
    console.log("Token deployed to:", currentImplAddress);
}

main();