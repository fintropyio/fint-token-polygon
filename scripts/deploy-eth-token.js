const { ethers, upgrades } = require("hardhat");
const { getImplementationAddress, Manifest } = require('@openzeppelin/upgrades-core');

async function main() {
    const manifestClient = await Manifest.forNetwork(network.provider);
    const manifest = await manifestClient.read();
    const signer = (await ethers.getSigners())[0];
    const argData= {
        // mainnet
        1: {
            predicateProxy: "0x9923263fA127b3d1484cFD649df8f1831c2A74e4",
        },
        // goerli
        5: {
            predicateProxy: "0x37c3bfC05d5ebF9EBb3FF80ce0bd0133Bf221BC8",
        },
        // local
        31337: {
            predicateProxy: "0x37c3bfC05d5ebF9EBb3FF80ce0bd0133Bf221BC8",
        }

    }
    let proxyAddr;
    let net = await ethers.provider.getNetwork();

    if (manifest.proxies[0] && net.chainId!=31337) {
        proxyAddr = manifest.proxies[0].address;
        console.log("proxy already deployed, reusing!", proxyAddr);
        await upgrades.upgradeProxy(proxyAddr, await ethers.getContractFactory("FintToken"));
    } else {
        const Token = await ethers.getContractFactory("FintToken");
        const args = argData[net.chainId]
        const token = await upgrades.deployProxy(Token, [
            args.predicateProxy, 
    
        ]);
        await token.deployed();
        proxyAddr = token.address;

        console.log("Token proxy deployed to:", proxyAddr);
    }

    
    const currentImplAddress = await getImplementationAddress(ethers.provider, proxyAddr);
    console.log("Token deployed to:", currentImplAddress);
}

main();