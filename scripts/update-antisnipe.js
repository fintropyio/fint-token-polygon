const { ethers, upgrades } = require("hardhat");
const { getImplementationAddress, Manifest } = require('@openzeppelin/upgrades-core');

async function main() {
    const manifestClient = await Manifest.forNetwork(network.provider);
    const manifest = await manifestClient.read();
    const signer = (await ethers.getSigners())[0];
  
    let proxyAddr;
    let net = await ethers.provider.getNetwork();

    if (manifest.proxies[0]) {
        proxyAddr = manifest.proxies[0].address;
        console.log("proxy already deployed:", proxyAddr);

        Token = await ethers.getContractFactory("FintTokenPOS");
        token = await Token.attach(proxyAddr);

        console.log("updating antisnipe");
        await token.updateAntisnipeAndRestrictor(
            "0x2306b42Baca1b34770EA489F4eC2023f5340B0Ca",
            ethers.constants.AddressZero
        )
    }
}

main();