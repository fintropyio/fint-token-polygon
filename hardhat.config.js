require('dotenv').config()
require('@openzeppelin/hardhat-upgrades');
require("hardhat-watcher");
require("@nomiclabs/hardhat-etherscan");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  networks: {
    mumbai: {
      chainId: 80001,
      live: true,
      saveDeployments: true,
      tags: ["staging"],
      url: process.env.MUMBAI_RPC,
      accounts: [process.env.MUMBAI_DEPLOYER]
    },
    matic: {
      chainId: 137,
      live: true,
      saveDeployments: true,
      tags: ["production"],
      url: process.env.MATIC_RPC,
      accounts: [process.env.MATIC_DEPLOYER]
    },
    goerli: {
      chainId: 5,
      live: true,
      saveDeployments: true,
      tags: ["staging"],
      url: process.env.GOERLI_RPC,
      accounts: [process.env.GOERLI_DEPLOYER]
    },
    mainnet: {
      chainId: 1,
      live: true,
      saveDeployments: true,
      tags: ["production"],
      url: process.env.MAINNET_RPC,
      accounts: [process.env.MAINNET_DEPLOYER]
    }
  },
  solidity: "0.8.10",
  watcher: {
    compilation: {
      tasks: ["compile"],
    },
    test: {
      tasks: ["compile", "test"],
      files: ["./contracts", "./test"]
    }
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_KEY
  }
};
