// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20BurnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import "./interfaces/ILiquidityRestrictor.sol";
import "./interfaces/IAntisnipe.sol";

contract FintTokenPOS is
    Initializable,
    ERC20Upgradeable,
    ERC20BurnableUpgradeable,
    AccessControlUpgradeable,
    UUPSUpgradeable
{
    bytes32 public constant UPGRADER_ROLE = keccak256("UPGRADER_ROLE");
    bytes32 public constant DEPOSITOR_ROLE = keccak256("DEPOSITOR_ROLE");

    address public antisnipe;
    address public restrictor;

    bool public antisnipeEnabled;
    bool public liquidityRestrictionEnabled;

    event AntisnipeDisabled(uint256 timestamp, address user);
    event LiquidityRestrictionDisabled(uint256 timestamp, address user);

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() initializer {}

    function initialize(address _childChainManager, address _tokenOwner, address _antisnipe, address _restrictor)
        public
        initializer
    {
        __ERC20_init("Fintropy", "FINT");
        __ERC20Burnable_init();
        __AccessControl_init();
        __UUPSUpgradeable_init();

        _mint(_tokenOwner, 30000000 * 10**decimals());
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(UPGRADER_ROLE, msg.sender);
        _setupRole(DEPOSITOR_ROLE, _childChainManager);


        if (_antisnipe!=address(0)) {
            antisnipeEnabled = true;
            antisnipe = _antisnipe;
        }
        if (_restrictor!=address(0)) {
            liquidityRestrictionEnabled = true;
            restrictor = _restrictor;
        }
    }

    function updateAntisnipeAndRestrictor(address _antisnipe, address _restrictor) 
        public
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        if (_antisnipe!=address(0)) {
            antisnipeEnabled = true;
            antisnipe = _antisnipe;
        }
        if (_restrictor!=address(0)) {
            liquidityRestrictionEnabled = true;
            restrictor = _restrictor;
        }
    }

    function _authorizeUpgrade(address newImplementation)
        internal
        override
        onlyRole(UPGRADER_ROLE)
    {}

    /**
     * @notice called when token is deposited on root chain
     * @dev Should be callable only by ChildChainManager
     * Should handle deposit by minting the required amount for user
     * Make sure minting is done only by this function
     * @param user user address for whom deposit is being done
     * @param depositData abi encoded amount
     */
    function deposit(address user, bytes calldata depositData)
        external
        onlyRole(DEPOSITOR_ROLE)
    {
        uint256 amount = abi.decode(depositData, (uint256));
        _mint(user, amount);
    }

    /**
     * @notice called when user wants to withdraw tokens back to root chain
     * @dev Should burn user's tokens. This transaction will be verified when exiting on root chain
     * @param amount amount of tokens to withdraw
     */
    function withdraw(uint256 amount) external {
        _burn(_msgSender(), amount);
    }

    // antisnipe and liquidity restrictor requires this
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override {
        if (from == address(0) || to == address(0)) return;
        if (liquidityRestrictionEnabled) {
            (bool allow, string memory message) = ILiquidityRestrictor(
                restrictor
            ).assureLiquidityRestrictions(from, to);
            require(allow, message);
        }

        if (antisnipeEnabled) {
            require(
                IAntisnipe(antisnipe).assureCanTransfer(
                    msg.sender,
                    from,
                    to,
                    amount
                )
            );
        }
    }

    function setAntisnipeDisable() external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(antisnipeEnabled);
        antisnipeEnabled = false;
        emit AntisnipeDisabled(block.timestamp, msg.sender);
    }

    function setLiquidityRestrictorDisable() external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(liquidityRestrictionEnabled);
        liquidityRestrictionEnabled = false;
        emit LiquidityRestrictionDisabled(block.timestamp, msg.sender);
    }
}
